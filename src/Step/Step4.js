import React from 'react'

import Question from '../Question'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import DatePicker from 'antd/lib/date-picker'
import Select from 'antd/lib/select'

import enUS from 'antd/lib/date-picker/locale/en_US'
import moment from 'moment'
moment.locale('en')

const { MonthPicker } = DatePicker
const { Option } = Select

const FormItem = Form.Item

export default class Step4 extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <Question q='My keywords' k='q41'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Keywords</span>
                  {getFieldDecorator('keywords', {
                    rules: [
                      { required: true, message: 'Please input your keywords!' },
                      {
                        validator: (rule, value, callback, source, options) => {
                          var errors = []
                          if (value && value.length > 3) {
                            errors.push('Maximum 3 keywords is allowed!')
                          }
                          callback(errors)
                        }
                      }
                    ],
                    initialValue: initialValue['keywords']
                  })(
                    <Select
                      tags
                      disabled={!edit}
                      style={{ minWidth: 120, width: 'initial', maxWidth: 240 }}
                      tokenSeparators={[',']}
                    >
                      <Option value='listening'>Listening</Option>
                      <Option value='perseverant'>Perseverant</Option>
                      <Option value='optimistic'>Optimistic</Option>
                    </Select>
                )}
                </span>
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='My links' k='q42'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                >Service</span>
                  {getFieldDecorator('links', {
                    rules: [
                      { required: true, message: 'Please input your links!' }
                    ],
                    initialValue: initialValue['links']
                  })(
                    <Select
                      disabled={!edit}
                      style={{ width: 120 }}
                    >
                      <Option value='linkedin'>Linked</Option>
                      <Option value='vimeo'>Vimeo</Option>
                      <Option value='facebook'>Facebook</Option>
                      <Option value='twitter'>Twitter</Option>
                    </Select>
                )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('url', {
                  rules: [{ required: true, message: 'Please input your url!' }],
                  initialValue: initialValue['url']
                })(
                  <Input disabled={!edit} addonBefore={<span>Url</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
      </div>
    )
  }
}

Step4.propTypes = {
}
