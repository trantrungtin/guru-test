import React from 'react'

import Question from '../Question'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import DatePicker from 'antd/lib/date-picker'
import Select from 'antd/lib/select'
import Rate from 'antd/lib/rate'

import enUS from 'antd/lib/date-picker/locale/en_US'
import moment from 'moment'
moment.locale('en')

const { MonthPicker } = DatePicker
const { Option } = Select

const FormItem = Form.Item

const ratingDescription = ['Concept', 'Beginner', 'Good', 'Very good', 'Excellent']

export default class Step2 extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <Question q='Skill && Licenses' k='q21'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields, getFieldValue } = form
            const ratingValue = getFieldValue('rating')
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('typeOfSkill', {
                  rules: [{ required: true, message: 'Please input your type of skill!' }],
                  initialValue: initialValue['typeOfSkill']
                })(
                  <Input disabled={!edit} addonBefore={<span>Type of skill</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('description', {
                  rules: [{ required: true, message: 'Please input your description!' }],
                  initialValue: initialValue['description']
                })(
                  <Input disabled={!edit} addonBefore={<span>description</span>} />
                )}
              </FormItem>
              <FormItem>
                <span>
                  <span
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Rating&nbsp;&nbsp;</span>
                  {getFieldDecorator('rating', {
                    rules: [{ required: true, message: 'Please set your rating!' }],
                    initialValue: initialValue['rating']
                  })(
                    <Rate />
                  )}
                  {ratingValue && <span className='ant-rate-text'>{ratingDescription[ratingValue - 1]}</span>}
                </span>
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Driving license' k='q22'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Type of license</span>
                  {getFieldDecorator('typeOfLicense', {
                    rules: [{ required: true, message: 'Please input your license type!' }],
                    initialValue: initialValue['typeOfLicense']
                  })(
                    <Select disabled={!edit} style={{ width: 120 }}>
                      <Option value='car'>Car</Option>
                      <Option value='van'>Van</Option>
                      <Option value='motorcycle'>Motorcycle</Option>
                    </Select>
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('category', {
                  rules: [{ required: true, message: 'Please input your category!' }],
                  initialValue: initialValue['category']
                })(
                  <Input disabled={!edit} addonBefore={<span>Category</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
      </div>
    )
  }
}

Step2.propTypes = {
}
