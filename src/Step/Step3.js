import React from 'react'

import Question from '../Question'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import InputNumber from 'antd/lib/input-number'
import DatePicker from 'antd/lib/date-picker'
import Select from 'antd/lib/select'

import enUS from 'antd/lib/date-picker/locale/en_US'
import moment from 'moment'
moment.locale('en')

const { MonthPicker } = DatePicker
const { Option } = Select

const FormItem = Form.Item

export default class Step3 extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <Question q='Hobbies' k='q31'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Type of hobby</span>
                  {getFieldDecorator('typeOfHobby', {
                    rules: [{ required: true, message: 'Please input your hobby type!' }],
                    initialValue: initialValue['typeOfHobby']
                  })(
                    <Select disabled={!edit} style={{ width: 120 }}>
                      <Option value='travel'>Travel</Option>
                      <Option value='music'>Music</Option>
                      <Option value='sport'>Sport</Option>
                      <Option value='reading'>Reading</Option>
                      <Option value='cinema'>Cinema</Option>
                      <Option value='art'>Art</Option>
                      <Option value='night-life'>Night-life</Option>
                      <Option value='other'>Other</Option>
                    </Select>
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('description', {
                  rules: [{ required: true, message: 'Please input your description!' }],
                  initialValue: initialValue['description']
                })(
                  <Input disabled={!edit} addonBefore={<span>Description</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Trip and experience overseas' k='q32'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('country', {
                  rules: [{ required: true, message: 'Please input your country!' }],
                  initialValue: initialValue['country']
                })(
                  <Input disabled={!edit} addonBefore={<span>Country</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('city', {
                  rules: [{ required: true, message: 'Please input your city!' }],
                  initialValue: initialValue['city']
                })(
                  <Input disabled={!edit} addonBefore={<span>City</span>} />
                )}
              </FormItem>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Start month</span>
                  {getFieldDecorator('startMonth', {
                    rules: [{ type: 'object', required: true, message: 'Please input your start month!' }],
                    initialValue: initialValue['startMonth'] || moment().locale('en').utcOffset(0)
                  })(
                    <MonthPicker
                      disabled={!edit}
                      allowClear={false}
                      placeholder='Start month'
                      locale={enUS}
                    />
                  )}
                </span>
              </FormItem>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Duration in week</span>
                  {getFieldDecorator('durationInWeek', {
                    rules: [{ required: true, message: 'Please input your duration in week!' }],
                    initialValue: initialValue['durationInWeek']
                  })(
                    <InputNumber min={1} disabled={!edit} />
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('typeOfActivity', {
                  rules: [{ required: true, message: 'Please input your type of activity!' }],
                  initialValue: initialValue['typeOfActivity']
                })(
                  <Input disabled={!edit} addonBefore={<span>Type of activity</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Cultural, sports, social activities' k='q33'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('typeOfActivity', {
                  rules: [{ required: true, message: 'Please input your type of activity!' }],
                  initialValue: initialValue['typeOfActivity']
                })(
                  <Input disabled={!edit} addonBefore={<span>Type of activity</span>} />
                )}
              </FormItem>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Duration in year</span>
                  {getFieldDecorator('durationInYear', {
                    rules: [{ required: true, message: 'Please input your duration in year!' }],
                    initialValue: initialValue['durationInYear']
                  })(
                    <InputNumber min={1} max={99} disabled={!edit} />
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('descriptionOfActivity', {
                  rules: [{ required: true, message: 'Please input your description of activity!' }],
                  initialValue: initialValue['descriptionOfActivity']
                })(
                  <Input disabled={!edit} addonBefore={<span>Description of activity</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('responsibilitiesAndAchievement', {
                  rules: [{ required: true, message: 'Please input your responsibilities / achievement!' }],
                  initialValue: initialValue['responsibilitiesAndAchievement']
                })(
                  <Input disabled={!edit} addonBefore={<span>Responsibilities / Achievement</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
      </div>
    )
  }
}

Step3.propTypes = {
}
