import React from 'react'

import Question from '../Question'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import DatePicker from 'antd/lib/date-picker'
import Select from 'antd/lib/select'

import enUS from 'antd/lib/date-picker/locale/en_US'
import moment from 'moment'
moment.locale('en')

const { MonthPicker } = DatePicker
const { Option } = Select

const FormItem = Form.Item

export default class Step1 extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <Question q='Additional Courses/ Certificates' k='q11' >
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Start month</span>
                  {getFieldDecorator('startMonth', {
                    rules: [{ type: 'object', required: true, message: 'Please input your start month!' }],
                    initialValue: initialValue['startMonth'] || moment().locale('en').utcOffset(0)
                  })(
                    <MonthPicker
                      disabled={!edit}
                      allowClear={false}
                      placeholder='Start month'
                      locale={enUS}
                    />
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('duration', {
                  rules: [{ required: true, message: 'Please input your duration!' }],
                  initialValue: initialValue['duration']
                })(
                  <Input disabled={!edit} addonBefore={<span>Duration</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('courseName', {
                  rules: [{ required: true, message: 'Please input your course name!' }],
                  initialValue: initialValue['courseName']
                })(
                  <Input disabled={!edit} addonBefore={<span>Course name</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('schoolName', {
                  rules: [{ required: true, message: 'Please input your school name!' }],
                  initialValue: initialValue['schoolName']
                })(
                  <Input disabled={!edit} addonBefore={<span>School name</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('location', {
                  rules: [{ required: true, message: 'Please input your location!' }],
                  initialValue: initialValue['location']
                })(
                  <Input disabled={!edit} addonBefore={<span>Location</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Professional Experience' k='q12' >
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Start month</span>
                  {getFieldDecorator('startMonth', {
                    rules: [{ type: 'object', required: true, message: 'Please input your start month!' }],
                    initialValue: initialValue['startMonth'] || moment().locale('en').utcOffset(0)
                  })(
                    <MonthPicker
                      disabled={!edit}
                      allowClear={false}
                      placeholder='Start month'
                      locale={enUS}
                    />
                  )}
                </span>
              </FormItem>
              <FormItem>
                {getFieldDecorator('jobTitle', {
                  rules: [{ required: true, message: 'Please input your job title!' }],
                  initialValue: initialValue['jobTitle']
                })(
                  <Input disabled={!edit} addonBefore={<span>Job title</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('companyName', {
                  rules: [{ required: true, message: 'Please input your company name!' }],
                  initialValue: initialValue['companyName']
                })(
                  <Input disabled={!edit} addonBefore={<span>Company name</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('location', {
                  rules: [{ required: true, message: 'Please input your location!' }],
                  initialValue: initialValue['location']
                })(
                  <Input disabled={!edit} addonBefore={<span>Location</span>} />
                )}
              </FormItem>
              <FormItem>
                <span>
                  <span
                    className='ant-input-group-addon'
                    style={{ lineHeight: '1rem', verticalAlign: 'initial' }}
                  >Contract type</span>
                  {getFieldDecorator('contractType', {
                    rules: [{ required: true, message: 'Please input your contract type!' }],
                    initialValue: initialValue['contractType']
                  })(
                    <Select disabled={!edit} style={{ width: 120 }}>
                      <Option value='cdd'>CDD</Option>
                      <Option value='stage'>Stage</Option>
                      <Option value='cdi'>CDI</Option>
                      <Option value='anternance'>ALTERNANCE)</Option>
                    </Select>
                  )}
                </span>
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
      </div>
    )
  }
}

Step1.propTypes = {
}
