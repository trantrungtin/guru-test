import React from 'react'

import Question from '../Question'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import Upload from 'antd/lib/upload'
import Button from 'antd/lib/button'
import Icon from 'antd/lib/icon'
import Radio from 'antd/lib/radio'

const RadioGroup = Radio.Group

const FormItem = Form.Item

export default class Step5 extends React.Component {
  constructor (props) {
    super(props)
  }

  normFile (e) {
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }

  render () {
    return (
      <div>
        <Question q='Add a video' k='q51'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('url', {
                  rules: [{ required: true, message: 'Please input your video url!' }],
                  initialValue: initialValue['url']
                })(
                  <Input disabled={!edit} addonBefore={<span>Video url</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('description', {
                  rules: [{ required: true, message: 'Please input your video description!' }],
                  initialValue: initialValue['description']
                })(
                  <Input disabled={!edit} addonBefore={<span>Video description</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Add a photo' k='q52'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields, getFieldValue } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('photos', {
                  valuePropName: 'fileList',
                  normalize: this.normFile,
                  rules: [{
                    validator: (rule, value, callback, source, options) => {
                      var errors = []
                      if (!value || !value.length) {
                        errors.push('Please upload at least 1 photo')
                      }
                      callback(errors)
                    }
                  }],
                  initialValue: initialValue['photos'] || []
                })(
                  <Upload name='photos' action='https://reqres.in/api/users' listType='picture'>
                    <Button disabled={!edit} type='ghost'>
                      <Icon type='upload' /> Click to upload photo
                    </Button>
                  </Upload>
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Add a document' k='q53'>
          {({ edit, error, done, onSubmit, handlerRenderer, form, initialValue, answerIndex }) => {
            const { getFieldDecorator, validateFields, getFieldValue } = form
            return <Form inline onSubmit={(e) => {
              e.preventDefault()
              validateFields((err, val) => {
                if (!err) {
                  onSubmit(val, answerIndex)
                }
              })
            }}>
              <FormItem>
                {getFieldDecorator('documents', {
                  valuePropName: 'fileList',
                  normalize: this.normFile,
                  rules: [{
                    validator: (rule, value, callback, source, options) => {
                      var errors = []
                      if (!value || !value.length) {
                        errors.push('Please upload at least 1 document')
                      }
                      callback(errors)
                    }
                  }],
                  initialValue: initialValue['documents'] || []
                })(
                  <Upload name='documents' action='https://reqres.in/api/users' listType='picture'>
                    <Button disabled={!edit} type='ghost'>
                      <Icon type='upload' /> Click to upload document
                    </Button>
                  </Upload>
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Please input your document name!' }],
                  initialValue: initialValue['name']
                })(
                  <Input disabled={!edit} addonBefore={<span>Document name</span>} />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('description', {
                  rules: [{ required: true, message: 'Please input your document description!' }],
                  initialValue: initialValue['description']
                })(
                  <Input disabled={!edit} addonBefore={<span>Document description</span>} />
                )}
              </FormItem>
              {handlerRenderer()}
            </Form>
          }}
        </Question>
        <Question q='Select your template' k='q54' singleSelection initialValue={'a'}>
          {({ onSubmit, value }) => {
            return <RadioGroup
              onChange={(e) => {
                onSubmit(oldValue => e.target.value)
              }}
              value={value}
            >
              <Radio value={'a'}>Template A</Radio>
              <Radio value={'b'}>Template B</Radio>
              <Radio value={'c'}>Template C</Radio>
              <Radio value={'d'}>Template D</Radio>
            </RadioGroup>
          }}
        </Question>
      </div>
    )
  }
}

Step5.propTypes = {
}
