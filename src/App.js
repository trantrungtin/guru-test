import React, { Component, PropTypes } from 'react'
import logo from './logo.svg'
import './App.css'

import { Step1, Step2, Step3, Step4, Step5 } from './Step'

import { Steps, Button, message } from 'antd'

import JSONPretty from 'react-json-pretty'
import 'react-json-pretty/src/JSONPretty.monikai.css'

const Result = ({ store }) => {
  const state = store.getState()
  return <JSONPretty json={state} />
}

const steps = [{
  title: 'Experiences',
  element: Step1
}, {
  title: 'Licenses',
  element: Step2
}, {
  title: 'Hobbies',
  element: Step3
}, {
  title: 'Keywords',
  element: Step4
}, {
  title: 'Templates',
  element: Step5
}
]

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      current: 0
    }

    this.validateQuestion = this.validateQuestion.bind(this)

    this.prev = () => this.setState({ current: this.state.current - 1 })
    this.next = () => this.validateQuestion(() => this.setState({ current: this.state.current + 1 }))
  }

  validateQuestion (cb) {
    console.log('validate')
    const state = this.context.store.getState()
    const unansweredQuestion = Object.keys(state).filter(k => k.startsWith(`q${this.state.current + 1}`))
                                      .map(k => state[k])
                                      .filter(answerData => {
                                        console.log(answerData)
                                        if (!answerData) {
                                          return true
                                        }
                                        if (Array.isArray(answerData.answers)) {
                                          return answerData.answers.length === 0
                                        } else {
                                          return !answerData.answers
                                        }
                                      })

    if (unansweredQuestion.length !== 0) {
      message.error(`Please complete these question: ${unansweredQuestion.map(data => data.title).join(', ')}`, 10)
    } else {
      cb()
    }
  }

  render () {
    const { current } = this.state
    return (
      <div className='App'>
        <div className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h2>trungtindev@gmail.com</h2>
        </div>
        <br />
        <div className='container'>
          <div className='Step-bar'>
            <Steps current={current}>
              {steps.map(item => <Steps.Step key={item.title} title={item.title} />)}
            </Steps>
          </div>
          <div>
            {current < steps.length
              ? React.createElement(steps[current].element)
              : <Result store={this.context.store} />
            }
          </div>
          <br />
          <div >
            {
              this.state.current < steps.length - 1 &&
              <Button type='primary' onClick={this.next}>Next</Button>
            }
            {
              this.state.current === steps.length - 1 &&
              <Button type='primary' onClick={this.next}>View result</Button>
            }
            {
              this.state.current > 0 &&
              <Button style={{ marginLeft: 8 }} type='ghost' onClick={this.prev}>
                Previous
              </Button>
            }
          </div>
        </div>
      </div>
    )
  }
}

App.contextTypes = {
  store: PropTypes.object
}

export default App
