import React, {PropTypes} from 'react'

import Button from 'antd/lib/button'
import Form from 'antd/lib/form'

import './Answer.css'

class Answer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      edit: props.answerIndex === -1,
      error: false,
      done: false
    }
    this.startEdit = this.startEdit.bind(this)
    this.cancelEdit = () => {
      this.setState({ edit: false })
      if (props.answerIndex === -1) {
        props.cancelAddAnswer()
      }
    }
    this.deleteAnswer = () => { props.deleteAnswer(props.answerIndex) }
  }

  startEdit () {
    this.setState({ edit: true, mo: 1 })
  }

  render () {
    const {
      edit, error, done
    } = this.state
    const { children, onSubmit, form, initialValue, answerIndex } = this.props
    if (typeof children !== 'function') {
      throw Error('Type of Answer children should be an function')
    }
    return <div>
      {
        children({
          edit,
          error,
          done,
          onSubmit: (val, ind) => {
            this.setState({ edit: false }, () => onSubmit(val, ind))
          },
          form,
          initialValue,
          answerIndex,
          handlerRenderer: () => edit
            ? <span className='answer-handler'>
              <Button key='submit' type='primary' shape='circle' icon='check' htmlType='submit' />
              <Button key='cancelEdit' shape='circle' icon='close' onClick={this.cancelEdit} />
            </span>
            : <span className='answer-handler'>
              <Button key='edit' type='primary' shape='circle' icon='edit' onClick={this.startEdit} />
              <Button key='delete' shape='circle' icon='delete' onClick={this.deleteAnswer} />
            </span>
        })
      }

    </div>
  }
}

Answer.propTypes = {
}

export default Form.create()(Answer)
