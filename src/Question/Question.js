import React, {PropTypes} from 'react'

import Answer from '../Answer'
import Button from 'antd/lib/button'

import './Question.css'

import { connect } from 'react-redux'

const mapStateToProps = (state, ownProps) => ({ initialValue: (state[ownProps.k] || {}).answers || ownProps.initialValue, storeValue: (state[ownProps.k] || {}).answers })

const setDataToStore = (k, d) => {
  return {
    type: 'SET',
    k,
    d
  }
}
const mapDispatchToProps = ({ setDataToStore })

class Question extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: props.initialValue || [],
      addingAnswer: false
    }
    this.addAnswer = this.addAnswer.bind(this)
    this.deleteAnswer = this.deleteAnswer.bind(this)
    this.cancelAddAnswer = () => {
      this.setState({ addingAnswer: false })
    }
    this.onAnswerSubmit = (val, answerIndex) => {
      let newData
      if (answerIndex === -1) {
        newData = this.state.data.concat(val)
        this.setState({ data: newData, addingAnswer: false })
      } else {
        const { data: newData } = this.state
        newData[answerIndex] = val
        this.setState({ data: newData })
      }

      this.props.setDataToStore(this.props.k, {
        title: this.props.q,
        answers: newData
      })
    }

    this.setAnswer = dataSetter => {
      if (typeof dataSetter !== 'function') {
        throw Error('setAnswer must be called with an setter function at first argument')
      }
      const newData = dataSetter(this.state.data)
      this.setState({ data: newData })
      this.props.setDataToStore(this.props.k, {
        title: this.props.q,
        answers: newData
      })
    }
  }

  componentDidMount () {
    if (!this.props.storeValue || this.props.storeValue !== this.props.initialValue) {
      this.props.setDataToStore(this.props.k, {
        title: this.props.q,
        answers: this.state.data
      })
    }
  }

  addAnswer () {
    this.setState({ addingAnswer: true })
  }

  deleteAnswer (answerIndex) {
    const { data } = this.state
    data[answerIndex] = null
    this.setState({ data })
  }

  render () {
    const { children, q, singleSelection } = this.props
    const { data, addingAnswer } = this.state
    if (singleSelection) {
      return <div>
        <div className='question-title'><b>{q}</b></div>
        {children({ onSubmit: this.setAnswer, value: this.state.data })}
      </div>
    }
    return (<div>
      <div className='question-title'><b>{q}</b></div>
      {
        data.map((d, i) => {
          if (!d) {
            return
          }
          return <Answer key={i} deleteAnswer={this.deleteAnswer} onSubmit={this.onAnswerSubmit} initialValue={d} answerIndex={i}>{children}</Answer>
        })
      }
      {addingAnswer
        ? <Answer key={-1} onSubmit={this.onAnswerSubmit} answerIndex={-1} initialValue={{}} cancelAddAnswer={this.cancelAddAnswer}>{children}</Answer>
        : <div>
          <Button onClick={this.addAnswer}>+ Add answer</Button>
        </div>
      }
    </div>)
  }
}

Question.propTypes = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Question)
