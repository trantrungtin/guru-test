import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './index.css'
import '../node_modules/antd/dist/antd.css'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import enUS from 'antd/lib/date-picker/locale/en_US'
import moment from 'moment'
moment.locale('en')

function reducer (state = {}, action) {
  switch (action.type) {
    case 'SET':
      return {
        ...state,
        [action.k]: action.d
      }
    default:
      return state
  }
}

const store = createStore(reducer)

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.getElementById('root')
)
